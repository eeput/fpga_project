module spi(
    input clk,
    input rst_n
);
    
    wire sclk, ss_n;
    wire mosi, miso;
    
    spi_master spi_master(
        .clk(clk),
        .rst_n(rst_n),
        .sclk(sclk),
        .ss_n(ss_n),
        .mosi(mosi),
        .miso(miso)
    );
    
    spi_slave spi_slave(
        .clk(clk),
        .rst_n(rst_n),
        .sclk(sclk),
        .ss_n(ss_n),
        .mosi(mosi),
        .miso(miso)
    );
    
endmodule 