`timescale 1ns/1ps

module spi_tb;
    
    reg clk, rst_n;
    
    initial begin
        clk = 0;
        rst_n = 0;
        #200.1 rst_n = 1;
    end 
    
    always #10 clk = ~clk;
    
    spi dut(
        .clk(clk),
        .rst_n(rst_n)
    );
    
endmodule 