`timescale 1ns/1ps

module bcd_tb;
    
    reg  [7 :0] bin;
    wire [11:0] bcd;
    integer i;
    
    bcd dut(.bin(bin), .bcd(bcd));
    
    initial begin
        bin = 0;
        
        for(i=0;i<=255;i=i+1) #10 bin = i;
        for(i=0;i<=255;i=i+1) #10 bin = i;
        for(i=0;i<=255;i=i+1) #10 bin = i;
        for(i=0;i<=255;i=i+1) #10 bin = i;
        
        #100 $stop;
    end
    
endmodule 