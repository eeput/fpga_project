module bcd_single_modify(data_in, data_out);
    
    input [3:0] data_in;
    output reg [3:0] data_out;
   
    always@(data_in)
    begin
        if(data_in > 4)
            data_out = data_in + 3;
        else
            data_out = data_in;
    end
    
endmodule 