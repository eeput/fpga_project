module bcd_modify(data_in, data_out);

    input  [19:0] data_in;
    output [19:0] data_out;
    wire   [19:0] reg_out;
    
    bcd_single_modify bcd2(.data_in(data_in[19:16]), .data_out(reg_out[19:16]));
    bcd_single_modify bcd1(.data_in(data_in[15:12]), .data_out(reg_out[15:12]));
    bcd_single_modify bcd0(.data_in(data_in[11:8]), .data_out(reg_out[11:8]));
    
    assign reg_out[7:0] = data_in[7:0];
    assign data_out = {reg_out[18:0], 1'b0};

endmodule 