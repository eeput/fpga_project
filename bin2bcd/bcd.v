module bcd(bin, bcd);

    input  [7 :0] bin;
    output [11:0] bcd;
    wire   [19:0] shift_reg [7:0];
    
    assign shift_reg[7] = {11'b0, bin, 1'b0};
    
    bcd_modify b7(.data_in(shift_reg[7]), .data_out(shift_reg[6]));
    bcd_modify b6(.data_in(shift_reg[6]), .data_out(shift_reg[5]));
    bcd_modify b5(.data_in(shift_reg[5]), .data_out(shift_reg[4]));
    bcd_modify b4(.data_in(shift_reg[4]), .data_out(shift_reg[3]));
    bcd_modify b3(.data_in(shift_reg[3]), .data_out(shift_reg[2]));
    bcd_modify b2(.data_in(shift_reg[2]), .data_out(shift_reg[1]));
    bcd_modify b1(.data_in(shift_reg[1]), .data_out(shift_reg[0]));
    
    assign bcd = shift_reg[0][19:8];

endmodule 