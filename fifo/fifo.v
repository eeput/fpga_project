module fifo(clk, rst_n, q);

    input clk;
    input rst_n;
    output [7:0] q;

    wire wrfull;
    wire rdempty;
    wire rdreq;
    wire wrreq;
    wire [7:0] data;

    rd_fifo rd_fifo(
        .clk(clk),
        .rst_n(rst_n),
        .rdempty(rdempty),
        .rdreq(rdreq)
    );
    
    wr_fifo wr_fifo(
        .clk(clk),
        .rst_n(rst_n),
        .wrfull(wrfull),
        .data(data),
        .wrreq(wrreq)
    );
    
    my_fifo my_fifo_inst(
        .data(data),
        .rdclk(clk),
        .rdreq(rdreq),
        .wrclk(clk),
        .wrreq(wrreq),
        .q(q),
        .rdempty(rdempty),
        .wrfull(wrfull)
    );
    
endmodule 