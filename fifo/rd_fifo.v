module rd_fifo(clk, rst_n, rdempty, rdreq);

    input clk;
    input rst_n;
    input rdempty;
    output reg rdreq;

    always @(posedge clk or negedge rst_n)
    begin
        if(!rst_n)
            rdreq <= 0;
        else
            if(rdempty)
                rdreq <= 0;
            else
                rdreq <= 1;
    end
    
endmodule 