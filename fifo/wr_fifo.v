module wr_fifo(clk, rst_n, wrfull, data, wrreq);

    input clk;
    input rst_n;
    input wrfull;
    output reg wrreq;
    output reg [7:0] data;

    always @(posedge clk or negedge rst_n)
    begin
        if(!rst_n)
            begin
                wrreq <= 0;
                data <= 8'b0;
            end
        else
            begin
                if(wrfull)
                    wrreq <= 0;
                else
                    wrreq <= 1;
                if(wrreq==1 & data<255)
                    data <= data + 1;
                else
                    data <= 8'b0;
            end
    end
    
endmodule 