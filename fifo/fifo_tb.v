`timescale 1ns/1ps

module fifo_tb;

    reg clk;
    reg rst_n;
    wire [7:0] q;

    initial begin
        clk = 0;
        rst_n = 0;
        #200.1 rst_n = 1;
        #20000 $stop;
    end

    always #10 clk = ~clk;
    
    fifo fifo(
        .clk(clk),
        .rst_n(rst_n),
        .q(q)
    );
    
endmodule 