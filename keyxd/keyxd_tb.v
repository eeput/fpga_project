`timescale 1ns/1ps

module keyxd_tb;

    reg clk;
    reg rst_n;
    reg key_in;
    wire key_out;
    
    initial
        begin
            clk = 0;
            rst_n =0;
            key_in = 0;
            #200.1 rst_n = 1;
            
            #20  key_in = 1;
            #20  key_in = 0;
            #20  key_in = 1;
            #100 key_in = 0;
            #20  key_in = 1;
            #20  key_in = 0;
            #2000 $stop;
        end
    
    always #10 clk = ~clk;
    
    keyxd keyxd(
        .clk(clk),
        .rst_n(rst_n),
        .key_in(key_in),
        .key_out(key_out)
        );
    
endmodule 