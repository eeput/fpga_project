module keyxd(clk, rst_n, key_in, key_out);
    
    input clk;
    input rst_n;
    input key_in;
    output reg key_out;
    
    reg [1:0] key_reg;
    reg [31:0] counter;
    
    always @(posedge clk or negedge rst_n)
    begin
        if(!rst_n)
            begin
                key_out <= 0;
                key_reg <= 0;
                counter <= 0;
            end
        else
            begin
                key_reg[1] <= key_in;
                key_reg[0] <= key_reg[1];
                if(key_reg[0]^key_reg[1])
                    counter <= 0;
                else if(counter<49999)      //仿真时改为counter<2
                    counter <= counter + 1;
                else
                    key_out <= key_reg[0];
            end
    end
    
endmodule 