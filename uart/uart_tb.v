`timescale 1ns/1ps

module uart_tb;
    
    reg clk;
    reg rst_n;
    reg din;
    wire dout;
    
    initial begin
        clk = 0;
        rst_n = 0;
        din = 1;
        #200.1 rst_n = 1;
        #8680 din = 0; //start
        #8680 din = 0;
        #8680 din = 0;
        #8680 din = 1;
        #8680 din = 1;
        #8680 din = 0;
        #8680 din = 1;
        #8680 din = 1;
        #8680 din = 0;
        #8680 din = 0; //parity
        #8680 din = 1; //stop
    end 
    
    always #10 clk = ~clk;
    
    uart dut(
        .clk(clk),
        .rst_n(rst_n),
        .din(din),
        .dout(dout)
    );
    
endmodule 