`timescale 1ns/1ps

module pid_tb;

    reg                 clk;
    reg                 rst_n;
    // kp ki kd timecycle
    reg     [15 : 0]    kp;
    reg     [15 : 0]    ki;
    reg     [15 : 0]    kd;
    reg     [31 : 0]    ti;
    // data input and output
    reg     [15 : 0]    din;
    reg     [15 : 0]    dref;
    wire    [15 : 0]    dout;

    initial begin
        $dumpfile("tb.vcd");
        $dumpvars;
        clk <= 1'b1;
        rst_n <= 1'b0;
        kp <= 16'h0001;
        ki <= 16'h0001;
        kd <= 16'h0000;
        ti <= 32'h051EB852;
        din <= 16'h0000;
        dref <= 16'h1000;
        #200.1 rst_n <= 1'b1;
        #10000 din <= 16'h1000;
        #20000 $stop;
    end

    always #10 clk <= ~ clk;

    pid pid_dut(
        .clk(clk),
        .rst_n(rst_n),
        // kp ki kd timecycle
        .kp(kp),
        .ki(ki),
        .kd(kd),
        .ti(ti),
        // data input and output
        .din(din),
        .dref(dref),
        .dout(dout)
    );

endmodule 