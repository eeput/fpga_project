module lsm(clk, rst_n, clock, data_out, load, ldac);

    input clk, rst_n;
    output reg load;
    output reg ldac;
    output reg clock;
    output reg data_out;
    reg [10:0] count;
    reg [7:0] data;
    reg [1:0] a1a0;
    reg rng;
    
    always @(posedge clk or negedge rst_n)
    begin
        if(!rst_n)
            count <= 0;
        else
            if(count<553)
                count <= count + 1;
            else
                count <= 0;
    end
    
    always @(posedge clk or negedge rst_n)
    begin
        if(!rst_n)
            begin
                clock <= 0;
                data_out <= 0;
                data <= 8'b01010101;
                a1a0 <= 2'b11;
                rng <= 1;
                load <= 1;
                ldac <= 1;
            end
        else
            case(count)
                0  : begin clock <= 1; ldac <= 1; data_out <= a1a0[1]; end
                25 : begin clock <= 0; end
                50 : begin clock <= 1; data_out <= a1a0[0]; end
                75 : begin clock <= 0; end
                100: begin clock <= 1; data_out <= rng; end
                125: begin clock <= 0; end
                150: begin clock <= 1; data_out <= data[7]; end
                175: begin clock <= 0; end
                200: begin clock <= 1; data_out <= data[6]; end
                225: begin clock <= 0; end
                250: begin clock <= 1; data_out <= data[5]; end
                275: begin clock <= 0; end
                300: begin clock <= 1; data_out <= data[4]; end
                325: begin clock <= 0; end
                350: begin clock <= 1; data_out <= data[3]; end
                375: begin clock <= 0; end
                400: begin clock <= 1; data_out <= data[2]; end
                425: begin clock <= 0; end
                450: begin clock <= 1; data_out <= data[1]; end
                475: begin clock <= 0; end
                500: begin clock <= 1; data_out <= data[0]; end
                525: begin clock <= 0; end
                528: begin load <= 0; end
                541: begin load <= 1; ldac <= 0; end
                default: ;
            endcase
    end
    
endmodule 