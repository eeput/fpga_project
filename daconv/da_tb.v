`timescale 1ns/1ps

module da_tb;
    
    reg clk;
    reg rst_n;
    wire clock;
    wire data_out;
    wire load;
    wire ldac;
    
    initial begin
    clk = 0;
    rst_n = 0;
    
    #200.1 rst_n = 1;
    #20000 $stop;
    end
    
    always #10 clk = ~clk;
    
    lsm lsm(
        .clk(clk),
        .rst_n(rst_n),
        .clock(clock),
        .data_out(data_out),
        .load(load),
        .ldac(ldac)
    );
    
endmodule 