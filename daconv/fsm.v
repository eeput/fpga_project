module fsm(clk, rst_n, clock, data_out, load, ldac);

    input clk, rst_n;
    output reg load;
    output reg ldac;
    output reg clock;
    output reg data_out;
    reg [7:0] data;
    reg [1:0] a1a0;
    reg rng;
    reg [5:0] cnt;
    reg [3:0] state;
    
    always @(posedge clk or negedge rst_n)
    begin
        if(!rst_n)
            begin
                clock <= 0;
                data_out <= 0;
                data <= 8'b01010101;
                a1a0 <= 2'b11;
                rng <= 1;
                load <= 1;
                ldac <= 1;
                cnt <= 0;
                state <= 0;
            end
        else
            case(state)
                0: begin
                    clock <= 1;
                    ldac <= 1;
                    data_out <= a1a0[1];
                    state <= 1;
                end
                1: begin
                    if(cnt<24)
                        begin
                            cnt <=cnt + 1;
                            state <= 1;
                        end
                    else if(cnt<49)
                        begin
                            clock <= 0;
                            cnt <= cnt + 1;
                            state <= 1;
                        end
                    else
                        begin
                            clock <= 1;
                            data_out <= a1a0[0];
                            cnt <= 0;
                            state <= 2;
                        end
                end
                2: begin
                    if(cnt<24)
                        begin
                            cnt <=cnt + 1;
                            state <= 2;
                        end
                    else if(cnt<49)
                        begin
                            clock <= 0;
                            cnt <= cnt + 1;
                            state <= 2;
                        end
                    else
                        begin
                            clock <= 1;
                            data_out <= rng;
                            cnt <= 0;
                            state <= 3;
                        end
                end
                3: begin
                    if(cnt<24)
                        begin
                            cnt <=cnt + 1;
                            state <= 3;
                        end
                    else if(cnt<49)
                        begin
                            clock <= 0;
                            cnt <= cnt + 1;
                            state <= 3;
                        end
                    else
                        begin
                            clock <= 1;
                            data_out <= data[7];
                            cnt <= 0;
                            state <= 4;
                        end
                end
                4: begin
                    if(cnt<24)
                        begin
                            cnt <=cnt + 1;
                            state <= 4;
                        end
                    else if(cnt<49)
                        begin
                            clock <= 0;
                            cnt <= cnt + 1;
                            state <= 4;
                        end
                    else
                        begin
                            clock <= 1;
                            data_out <= data[6];
                            cnt <= 0;
                            state <= 5;
                        end
                end
                5: begin
                    if(cnt<24)
                        begin
                            cnt <=cnt + 1;
                            state <= 5;
                        end
                    else if(cnt<49)
                        begin
                            clock <= 0;
                            cnt <= cnt + 1;
                            state <= 5;
                        end
                    else
                        begin
                            clock <= 1;
                            data_out <= data[5];
                            cnt <= 0;
                            state <= 6;
                        end
                end
                6: begin
                    if(cnt<24)
                        begin
                            cnt <=cnt + 1;
                            state <= 6;
                        end
                    else if(cnt<49)
                        begin
                            clock <= 0;
                            cnt <= cnt + 1;
                            state <= 6;
                        end
                    else
                        begin
                            clock <= 1;
                            data_out <= data[4];
                            cnt <= 0;
                            state <= 7;
                        end
                end
                7: begin
                    if(cnt<24)
                        begin
                            cnt <=cnt + 1;
                            state <= 7;
                        end
                    else if(cnt<49)
                        begin
                            clock <= 0;
                            cnt <= cnt + 1;
                            state <= 7;
                        end
                    else
                        begin
                            clock <= 1;
                            data_out <= data[3];
                            cnt <= 0;
                            state <= 8;
                        end
                end
                8: begin
                    if(cnt<24)
                        begin
                            cnt <=cnt + 1;
                            state <= 8;
                        end
                    else if(cnt<49)
                        begin
                            clock <= 0;
                            cnt <= cnt + 1;
                            state <= 8;
                        end
                    else
                        begin
                            clock <= 1;
                            data_out <= data[2];
                            cnt <= 0;
                            state <= 9;
                        end
                end
                9: begin
                    if(cnt<24)
                        begin
                            cnt <=cnt + 1;
                            state <= 9;
                        end
                    else if(cnt<49)
                        begin
                            clock <= 0;
                            cnt <= cnt + 1;
                            state <= 9;
                        end
                    else
                        begin
                            clock <= 1;
                            data_out <= data[1];
                            cnt <= 0;
                            state <= 10;
                        end
                end
                10: begin
                    if(cnt<24)
                        begin
                            cnt <=cnt + 1;
                            state <= 10;
                        end
                    else if(cnt<49)
                        begin
                            clock <= 0;
                            cnt <= cnt + 1;
                            state <= 10;
                        end
                    else
                        begin
                            clock <= 1;
                            data_out <= data[0];
                            cnt <= 0;
                            state <= 11;
                        end
                end
                11: begin
                    if(cnt<24)
                        begin
                            cnt <=cnt + 1;
                            state <= 11;
                        end
                    else
                        begin
                            clock <= 0;
                            cnt <= 0;
                            state <= 12;
                        end
                end
                12: begin
                    if(cnt<2)
                        begin
                            cnt <= cnt + 1;
                            state <= 12;
                        end
                    else if(cnt<15)
                        begin
                            load <= 0;
                            cnt <= cnt + 1;
                            state <= 12;
                        end
                    else
                        begin
                            load <= 1;
                            ldac <= 0;
                            cnt <= 0;
                            state <= 13;
                        end
                end
                13: begin
                    if(cnt<12)
                        begin
                            cnt <= cnt + 1;
                            state <= 13;
                        end
                    else
                        begin
                            clock <= 1;
                            ldac <= 1;
                            data_out <= a1a0[1];
                            cnt <= 0;
                            state <= 1;
                        end
                end
                default: state <= 0;
            endcase
    end
    
endmodule 