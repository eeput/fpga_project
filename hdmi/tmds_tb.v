`timescale 1ns/1ps

module tmds_tb;

    reg        clk;
    reg        rst_n;
    reg        de;
    reg  [7:0] din;
    reg        hs;
    reg        vs;
    wire [9:0] dout;

    initial begin
        $dumpfile("tb.vcd");
        $dumpvars;
        clk <= 1'b1;
        rst_n <= 1'b0;
        de <= 1'b0;
        hs <= 1'b0;
        vs <= 1'b0;
        din <= 8'b00111100;
        #200.1 rst_n <= 1'b1;
        #200 de <= 1'b0; hs <= 1'b0; vs <= 1'b0;
        #200 de <= 1'b0; hs <= 1'b0; vs <= 1'b1;
        #200 de <= 1'b0; hs <= 1'b1; vs <= 1'b0;
        #200 de <= 1'b0; hs <= 1'b1; vs <= 1'b1;
        #200 de <= 1'b1; hs <= 1'b0; vs <= 1'b0;
        #200 de <= 1'b1; hs <= 1'b0; vs <= 1'b1;
        #200 de <= 1'b1; hs <= 1'b1; vs <= 1'b0;
        #200 de <= 1'b1; hs <= 1'b1; vs <= 1'b1;
        #200 de <= 1'b0; hs <= 1'b0; vs <= 1'b0;
        #200 $finish;
    end

    always #10 clk <= ~clk;

    tmds_coder dut(
        .clk(clk),
        .rst_n(rst_n),
        // TMDS interface
        .de(de),
        .d(din),
        .c0(hs),
        .c1(vs),
        .q_o(dout)
    );

endmodule 