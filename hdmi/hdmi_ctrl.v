module hdmi_ctrl(
        input        clk_l,
        input        clk_h,
        input        rst_n,
        // RGB image interface
        input        hs,
        input        vs,
        input        de,
        input  [7:0] red,
        input  [7:0] green,
        input  [7:0] blue,
        // HDMI interface
        output       clk_p,
        output       clk_n,
        output [2:0] dat_p,
        output [2:0] dat_n,
        output [0:0] oen
    );

    wire       creg;
    wire [2:0] dreg;
    wire [9:0] q0, q1, q2;

    assign oen = 1'b1;

    OBUFDS clk_pn_inst(
        .I(creg),
        .O(clk_p),
        .OB(clk_n)
    );

    OBUFDS dat0_pn_inst (
        .I(dreg[0]),
        .O(dat_p[0]),
        .OB(dat_n[0])
    );
    OBUFDS dat1_pn_inst (
        .I(dreg[1]),
        .O(dat_p[1]),
        .OB(dat_n[1])
    );
    OBUFDS dat2_pn_inst (
        .I(dreg[2]),
        .O(dat_p[2]),
        .OB(dat_n[2])
    );

    dat_serial serial_inst0(
        .data_out_from_device(10'h3E0),
        .data_out_to_pins(creg),
        .clk_in(clk_l),
        .clk_div_in(clk_h),
        .io_reset(!rst_n)
    );

    dat_serial serial_inst1(
        .data_out_from_device(q0),
        .data_out_to_pins(dreg[0]),
        .clk_in(clk_l),
        .clk_div_in(clk_h),
        .io_reset(!rst_n)
    );

    dat_serial serial_inst2(
        .data_out_from_device(q1),
        .data_out_to_pins(dreg[1]),
        .clk_in(clk_l),
        .clk_div_in(clk_h),
        .io_reset(!rst_n)
    );

    dat_serial serial_inst3(
        .data_out_from_device(q2),
        .data_out_to_pins(dreg[2]),
        .clk_in(clk_l),
        .clk_div_in(clk_h),
        .io_reset(!rst_n)
    );

    tmds_coder tmds_inst0(
        .clk(clk_l),
        .rst_n(rst_n),
        // TMDS interface
        .de(de),
        .d(blue),
        .c0(hs),
        .c1(vs),
        .q_o(q0)
    );

    tmds_coder tmds_inst1(
        .clk(clk_l),
        .rst_n(rst_n),
        // TMDS interface
        .de(de),
        .d(green),
        .c0(1'b0),
        .c1(1'b0),
        .q_o(q1)
    );

    tmds_coder tmds_inst2(
        .clk(clk_l),
        .rst_n(rst_n),
        // TMDS interface
        .de(de),
        .d(red),
        .c0(1'b0),
        .c1(1'b0),
        .q_o(q2)
    );

endmodule 