module led_run(
    input      clk,
    input      rst_n,
    // LED interface
    output reg led
);

    reg [31:0] cnt;

    always @(posedge clk, negedge rst_n) begin
        if (!rst_n) begin
            led <= 1'b1;
            cnt <= 32'b0;
        end
        else begin
            led <= cnt[31];
            cnt <= cnt + 8'h7F;
        end
    end

endmodule 