module color_dvi(
    input        clk,
    input        rst_n,
    output reg   hs,
    output reg   vs,
    output reg   de,
    output [7:0] red,
    output [7:0] green,
    output [7:0] blue
);

    parameter H_FPHA   = 16'd88;        // 行前段长度
    parameter H_SYNC   = 16'd44;        // 行同步长度
    parameter H_BPHA   = 16'd148;       // 行后段长度
    parameter H_ACTIVE = 16'd1920;      // 行有效长度
    parameter V_FPHA   = 16'd4;         // 帧前段长度
    parameter V_SYNC   = 16'd5;         // 帧同步长度
    parameter V_BPHA   = 16'd36;        // 帧后段长度
    parameter V_ACTIVE = 16'd1080;      // 帧有效长度
    parameter H_NUMS   = H_ACTIVE + H_FPHA + H_SYNC + H_BPHA;
    parameter V_NUMS   = V_ACTIVE + V_FPHA + V_SYNC + V_BPHA;

    reg [11:0] h_cnt;
    reg [11:0] v_cnt;
    reg hs_buf, vs_buf;
    reg he_reg, ve_reg;

    assign red   = 8'h00;
    assign green = 8'h00;
    assign blue  = 8'hff;

    always @(posedge clk, negedge rst_n) begin
        if (!rst_n) begin
            hs <= 1'b0;
            vs <= 1'b0;
            de <= 1'b0;
        end
        else begin
            hs <= hs_buf;
            vs <= vs_buf;
            de <= he_reg & ve_reg;
        end
    end

    always @(posedge clk, negedge rst_n) begin
        if (!rst_n) begin
            h_cnt <= 12'b0;
            v_cnt <= 12'b0;
        end
        else begin
            if (h_cnt < (H_NUMS-1))
                h_cnt <= h_cnt + 1'b1;
            else
                h_cnt <= 12'b0;
            if ((v_cnt < (V_NUMS-1)) & (h_cnt == (H_FPHA-1)))
                v_cnt <= v_cnt + 1'b1;
            else if (h_cnt == (H_FPHA-1))
                v_cnt <= 12'b0;
        end
    end

    always @(posedge clk, negedge rst_n) begin
        if (!rst_n) begin
            hs_buf <= 1'b0;
            vs_buf <= 1'b0;
        end
        else begin
            if (h_cnt == (H_FPHA-1))
                hs_buf <= 1'b1;
            else if (h_cnt == (H_FPHA+H_SYNC-1))
                hs_buf <= 1'b0;
            if ((v_cnt == (V_FPHA-1)) & (h_cnt == (H_FPHA-1)))
                vs_buf <= 1'b1;
            else if ((v_cnt == (V_FPHA+V_SYNC-1)) & (h_cnt == (H_FPHA-1)))
                vs_buf <= 1'b0;
        end
    end

    always @(posedge clk, negedge rst_n) begin
        if (!rst_n) begin
            he_reg <= 1'b0;
            ve_reg <= 1'b0;
        end
        else begin
            if (h_cnt == (H_FPHA+H_SYNC+H_BPHA-1))
                he_reg <= 1'b1;
            else if (h_cnt == (H_NUMS-1))
                he_reg <= 1'b0;
            if ((v_cnt == (V_FPHA+V_SYNC+V_BPHA-1)) & (h_cnt == (H_FPHA-1)))
                ve_reg <= 1'b1;
            else if ((v_cnt == (V_NUMS-1)) & (h_cnt == (H_FPHA-1)))
                ve_reg <= 1'b0;
        end
    end

endmodule 