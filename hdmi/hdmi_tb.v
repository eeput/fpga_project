`timescale 1ns/1ps

module hdmi_tb;

    reg        clk;
    reg        rst_n;
    wire       clk_p;
    wire       clk_n;
    wire [2:0] dat_p;
    wire [2:0] dat_n;
    wire [3:0] led;

    initial begin
        $dumpfile("tb.vcd");
        $dumpvars;
        clk <= 1'b1;
        rst_n <= 1'b0;
        #200.1 rst_n <= 1'b1;
        #20000 $finish;
    end

    always #10 clk <= ~clk;

    hdmi dut(
        .sys_clk(clk),
        .sys_rst_n(rst_n),
        // HDMI interface
        .hdmi_clk_p(clk_p),
        .hdmi_clk_n(clk_n),
        .hdmi_dat_p(dat_p),
        .hdmi_dat_n(dat_n),
        .hdmi_oen(oen),
        // LED interface
        .led_out(led)
    );

endmodule 