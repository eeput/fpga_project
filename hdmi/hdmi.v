module hdmi(
        input        sys_clk,
        input        sys_rst_n,
        // HDMI interface
        output [0:0] hdmi_clk_p,
        output [0:0] hdmi_clk_n,
        output [2:0] hdmi_dat_p,
        output [2:0] hdmi_dat_n,
        output [0:0] hdmi_oen,
        // LED interface
        output [3:0] led_out
    );

    wire       pll_clk_l;
    wire       pll_clk_h;
    wire       sof_rst_n;
    wire       hs;
    wire       vs;
    wire       de;
    wire [7:0] red;
    wire [7:0] green;
    wire [7:0] blue;

    clk_gener clk_inst(
        // Clock in ports
        .reset(!sys_rst_n),
        .locked(sof_rst_n),
        .clk_in1(sys_clk),
        // Clock out ports
        .clk_out1(pll_clk_l),
        .clk_out2(pll_clk_h)
    );

    color_dvi color_inst(
        .clk(pll_clk_l),
        .rst_n(sof_rst_n),
        .hs(hs),
        .vs(vs),
        .de(de),
        .red(red),
        .green(green),
        .blue(blue)
    );

    hdmi_ctrl hdmi_inst(
        .clk_l(pll_clk_l),
        .clk_h(pll_clk_h),
        .rst_n(sof_rst_n),
        // RGB image interface
        .hs(hs),
        .vs(vs),
        .de(de),
        .red(red),
        .green(green),
        .blue(blue),
        // HDMI interface
        .clk_p(hdmi_clk_p),
        .clk_n(hdmi_clk_n),
        .dat_p(hdmi_dat_p),
        .dat_n(hdmi_dat_n),
        .oen(hdmi_oen)
    );

    led_run led_inst(
        .clk(pll_clk_l),
        .rst_n(sof_rst_n),
        // LED interface
        .led(led_out[0])
    );

endmodule 