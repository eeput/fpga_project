module vga_control(clock, rst_n, hsync, vsync, address);
    
    input clock;
    input rst_n;
    output hsync;
    output vsync;
    output [14:0] address;
    
    reg [14:0] acount;
    reg [10:0] hcount;
    reg [10:0] vcount;
    wire encolor;
    
    always @(posedge clock or negedge rst_n)
    begin
        if(!rst_n)
            begin
                hcount <= 0;
                vcount <= 0;
            end
        else if(vcount<627)
            if(hcount<1055)
                hcount <= hcount + 1;
            else
                begin
                    hcount <= 0;
                    vcount <= vcount + 1;
                end
        else
            vcount <= 0;
    end
    
    always @(posedge clock or negedge rst_n)
    begin
        if(!rst_n)
            acount <= 0;
        else if(encolor)
            if(acount<19200)
                begin
                    if(!(hcount%5))
                        if((hcount>1010)&&(vcount%5))
                            acount <= acount - 159;
                        else
                            acount <= acount + 1;
                end
            else
                acount <= 0;
    end
    
    assign hsync = (hcount<127) ? 1'b0 : 1'b1;
    assign vsync = (vcount<3) ? 1'b0 : 1'b1;
    assign encolor = ((hcount>215 && hcount<1019)&(vcount>26 && vcount<627)) ? 1'b1 : 1'b0;
    assign address = encolor ? acount : 15'b0;
    
endmodule 