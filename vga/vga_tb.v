`timescale 1ns/1ps

module vga_tb;
    
    reg clk;
    reg rst_n;
    wire hsync;
    wire vsync;
    wire [7:0] color;
    
    vga dut(
        .clk(clk),
        .rst_n(rst_n),
        .hsync(hsync),
        .vsync(vsync),
        .color(color)
    );
    
    initial begin
        clk = 0;
        rst_n = 0;
        
        #200.1 rst_n = 1;
        #800000 $stop;
    end
    
    always #10 clk = ~clk;
    
endmodule 