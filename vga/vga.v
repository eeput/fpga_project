module vga(clk, rst_n, hsync, vsync, color);
    
    input clk;
    input rst_n;
    output hsync;
    output vsync;
    output [7:0] color;
    
    wire [14:0] address;
    wire clock;
    
    vga_control vga_control(
        .clock(clock),
        .rst_n(rst_n),
        .hsync(hsync),
        .vsync(vsync),
        .address(address)
    );
    
    rom_image rom_image(
        .address(address),
        .clock(clock),
        .q(color)
    );
    
        pll_clock pll_clock(
        .inclk0(clk),
        .c0(clock)
    );
    
endmodule 