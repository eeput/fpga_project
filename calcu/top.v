module top(clk, rst_n, row, col, sel, seg);

    input clk;
    input rst_n;
    input [3:0] row;
    output [3:0] col;
    output [2:0] sel;
    output [7:0] seg;
    
    wire [23:0] result;
    wire [4:0] key_num;
    wire [23:0] dout;
    wire [23:0] din;
    wire key_flag;
    
    seg8 seg8(
        .clk(clk),
        .rst_n(rst_n),
        .data(dout),
        .sel(sel),
        .seg(seg)
    );
    
    key_scan key_scan(
        .clk(clk),
        .rst_n(rst_n),
        .row(row),
        .col(col),
        .key_num(key_num),
        .key_flag(key_flag)
    );
    
    shift shift(
        .clk(clk),
        .rst_n(rst_n),
        .key_num(key_num),
        .key_flag(key_flag),
        .data(din)
    );
    
    alu alu(
        .clk(clk),
        .rst_n(rst_n),
        .data(din),
        .key_num(key_num),
        .result(result)
    );

    mux mux(
        .clk(clk),
        .rst_n(rst_n),
        .data(din),
        .result(result),
        .key_num(key_num),
        .data_out(dout)
    );
    
endmodule 