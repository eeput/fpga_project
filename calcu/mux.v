module mux(clk, rst_n, data, result, key_num, data_out);

    input clk;
    input rst_n;
    input [23:0] data;
    input [23:0] result;
    input [4:0] key_num;
    output reg [23:0] data_out;
    
    reg state;
    
    always @(posedge clk or negedge rst_n)
    begin
        if(!rst_n)
            begin
                data_out <= 0;
                state <= 0;
            end
        else
            case(state)
                0: begin
                    if(key_num==10)
                        state <= 1;
                    else
                        begin
                            state <= 0;
                            data_out <= data;
                        end
                end
                1: begin
                    data_out <= result;
                    if((key_num!=10) && (key_num!=16))
                        state <= 0;
                end
                default: state <= 0;
            endcase
    end
    
endmodule 