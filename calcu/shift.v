module shift(clk, rst_n, key_num, key_flag, data);

    input clk;
    input rst_n;
    input key_flag;
    input [4:0] key_num;
    output reg [23:0] data;
    
    reg state;
    
    always @(posedge clk or negedge rst_n)
    begin
        if(!rst_n)
            begin
                data <= 24'b0;
                state <= 0;
            end
        else
            case(state)
                0: begin
                    if(key_flag==1)
                        if((key_num>=10) && (key_num<=16))
                            data <= 0;
                        else
                            begin
                                state <= 1;
                                data <= {data[19:0], key_num[3:0]};
                            end
                end
                1: begin
                    if(key_flag==0)
                        state <= 0;
                end
            endcase
    end
    
endmodule 