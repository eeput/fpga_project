module alu(clk, rst_n, data, key_num, result);

    input clk;
    input rst_n;
    input [4:0] key_num;
    input [23:0] data;
    output reg [23:0] result;
    
    reg [1:0] state;
    reg [4:0] key_b;
    reg [23:0] a;
    reg [23:0] b;
    reg [23:0] c;
    
    always @(posedge clk or negedge rst_n)
    begin
        if(!rst_n)
            begin
                state <= 0;
                key_b <= 0;
                a <= 0;
                b <= 0;
                c <= 0;
            end
        else
            case(state)
                0: begin
                    if((key_num>=10) && (key_num<=15))
                        begin
                            if((key_num>=12) && (key_num<=15))
                                key_b <= key_num;
                            else
                                key_b <= 0;
                            state <= 1;
                        end
                    else
                        begin
                            a <= ((data[23:20]*100000)
                               + (data[19:16]* 10000)
                               + (data[15:12]*  1000)
                               + (data[11: 8]*   100)
                               + (data[ 7: 4]*    10)
                               + (data[ 3: 0]       ));
                            state <= 0;
                        end
                end
                1: begin
                    b <=((data[23:20]*100000)
                       + (data[19:16]* 10000)
                       + (data[15:12]*  1000)
                       + (data[11: 8]*   100)
                       + (data[ 7: 4]*    10)
                       + (data[ 3: 0]       ));
                    if((key_num==10) || (key_num==11))
                        state <= 2;
                end
                2: begin
                    if(key_b==12)
                        c <= b + a;
                    else if(key_b==13)
                        if(a>=b)
                            c <= a - b;
                        else
                            c <= b - a;
                    else if(key_b==14)
                        c <= b * a;
                    else if(key_b==15)
                        if((b==0)||(a==0))
                            c <= 0;
                        else
                            c <= a / b;
                    state <= 0;
                end
                default: state <= 0;
            endcase
    end 
    
    always @(*)
    begin
        if(!rst_n)
            result <= 0;
        else
            if(key_num==10)
                begin
                    result[23:20] = ( c/100000    );
                    result[19:16] = ((c/ 10000)%10);
                    result[15:12] = ((c/  1000)%10);
                    result[11: 8] = ((c/   100)%10);
                    result[ 7: 4] = ((c/    10)%10);
                    result[ 3: 0] = ( c        %10);
                end
    end 
    
endmodule 