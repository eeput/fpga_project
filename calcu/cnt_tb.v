`timescale 1ns/1ps

module cnt_tb;

    reg clk;
    reg rst_n;
    reg [3:0] row;
    
    wire [3:0] col;
    wire [2:0] sel;
    wire [7:0] seg;
    
    reg [4:0] pnumber;
    
    initial begin
        pnumber = 1;  #1000 pnumber = 16; #100
        pnumber = 12; #1000 pnumber = 16; #100
        pnumber = 6;  #1000 pnumber = 16; #100
        pnumber = 10; #1000 pnumber = 16;
    end
    
    initial begin
        clk = 0;
        rst_n = 0;
        #200.1 rst_n = 1;
        #20000 $stop;
    end
    
    always #10 clk = ~clk;
    
    always @(*)
    begin
        if(!rst_n)
            row = 4'b1111;
        else
            case(pnumber)
                0: row = {1'b1,1'b1,1'b1,col[0]};
                1: row = {1'b1,1'b1,1'b1,col[1]};
                2: row = {1'b1,1'b1,1'b1,col[2]};
                3: row = {1'b1,1'b1,1'b1,col[3]};
                
                4: row = {1'b1,1'b1,col[0],1'b1};
                5: row = {1'b1,1'b1,col[1],1'b1};
                6: row = {1'b1,1'b1,col[2],1'b1};
                7: row = {1'b1,1'b1,col[3],1'b1};
                
                8 : row = {1'b1,col[0],1'b1,1'b1};
                9 : row = {1'b1,col[1],1'b1,1'b1};
                10: row = {1'b1,col[2],1'b1,1'b1};
                11: row = {1'b1,col[3],1'b1,1'b1};
                
                12: row = {col[0],1'b1,1'b1,1'b1};
                13: row = {col[1],1'b1,1'b1,1'b1};
                14: row = {col[2],1'b1,1'b1,1'b1};
                15: row = {col[3],1'b1,1'b1,1'b1};
                default: row = 4'b1111;
            endcase
    end
    
    top top(
        .clk(clk),
        .rst_n(rst_n),
        .row(row),
        .col(col),
        .sel(sel),
        .seg(seg)
        );
    
endmodule 