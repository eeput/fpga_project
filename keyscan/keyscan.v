module keyscan(clk, rst_n, row, col, key_num);
    
    input clk;
    input rst_n;
    input [3:0] row;
    output reg [3:0] col;
    output reg [4:0] key_num;
    
    reg clk1K;
    reg [20:0] counter;
    reg [3:0] state;
    
    always @(posedge clk or negedge rst_n)
    begin
        if(!rst_n)
            begin
                counter <= 0;
                clk1K <= 0;
            end
        else
            if(counter<(50000000/1000/2-1))   //仿真时改为counter<2
                counter <= counter + 1'b1;
            else
                begin
                    counter <= 0;
                    clk1K <= ~clk1K;
                end
    end
    
    
    always @(posedge clk1K or negedge rst_n)
    begin
        if(!rst_n)
            begin
                state <= 0;
                col <= 4'b0000;
            end
        else
            case(state)
                0: begin
                    if(&row==0)
                        begin
                            col <= 4'b1110;
                            state <= 1;
                        end
                    else
                        begin
                            col <= 4'b0000;
                            state <= 0;
                        end
                end
                1: begin
                    if(&row==0)
                        begin
                            col <= 4'b0000;
                            state <= 5;
                        end
                    else
                        begin
                            col <= 4'b1101;
                            state <= 2;
                        end
                end
                2: begin
                    if(&row==0)
                        begin
                            col <= 4'b0000;
                            state <= 5;
                        end
                    else
                        begin
                            col <= 4'b1011;
                            state <= 3;
                        end
                end
                3: begin
                    if(&row==0)
                        begin
                            col <= 4'b0000;
                            state <= 5;
                        end
                    else
                        begin
                            col <= 4'b0111;
                            state <= 4;
                        end
                end
                4: begin
                    if(&row==0)
                        begin
                            col <= 4'b0000;
                            state <= 5;
                        end
                    else
                        state <= 0;
                end
                5: begin
                    if(&row)
                        state <= 0;
                    else
                        state <= 5;
                end
                default: state <= 0;
            endcase
    end
    
    always @(posedge clk1K or negedge rst_n)
    begin
        if(!rst_n)
            key_num = 0;
        else
            case({row,col})
                8'b1110_1110: key_num =  0;
                8'b1110_1101: key_num =  1;
                8'b1110_1011: key_num =  2;
                8'b1110_0111: key_num =  3;
                8'b1101_1110: key_num =  4;
                8'b1101_1101: key_num =  5;
                8'b1101_1011: key_num =  6;
                8'b1101_0111: key_num =  7;
                8'b1011_1110: key_num =  8;
                8'b1011_1101: key_num =  9;
                8'b1011_1011: key_num = 10;
                8'b1011_0111: key_num = 11;
                8'b0111_1110: key_num = 12;
                8'b0111_1101: key_num = 13;
                8'b0111_1011: key_num = 14;
                8'b0111_0111: key_num = 15;
                default: ;
            endcase
    end
    
endmodule 