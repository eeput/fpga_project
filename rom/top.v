module top(clk, rst_n, q);

    input clk;
    input rst_n;
    output [7:0] q;
    wire [7:0] address;

    rom_control rom_control(
        .clk(clk), 
        .rst_n(rst_n), 
        .address(address)
    );
    
    rom rom_inst(
        .address(address),
        .clock(clk),
        .q(q)
    );
    
endmodule 