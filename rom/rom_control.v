module rom_control(clk, rst_n, address);

    input clk;
    input rst_n;
    output reg [7:0] address;

    always @(posedge clk or negedge rst_n)
    begin
        if(!rst_n)
            address <= 0;
        else
            if(address<255)
                address <= address + 1;
            else
                address <= 0;
    end
    
endmodule 