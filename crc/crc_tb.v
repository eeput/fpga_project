`timescale 1ns/1ps

module crc_tb;

    reg clk, rst_n;
    reg sen, err;
    reg  [15 : 0] din;
    reg  [5  : 0] cin;
    wire [4  : 0] dout;

    initial begin
        clk <= 1'b0;
        rst_n <= 1'b0;
        #200.1 rst_n <= 1'b1;
        #20000 $stop;
    end

    always #10 clk <= ~ clk;

    always @(posedge clk, negedge rst_n) begin
        if (!rst_n) begin
            din <= 16'b0;
            cin <= 6'b100101;
            sen <= 1'b0;
            err <= 1'b0;
        end
        else begin
            sen <= ~ sen;
            if (sen) din[4 : 0] <= dout;
            else begin
                din[15 : 5] <= din[15 : 5] + 1'b1;
                din[4 : 0] <= 5'b0;
                if (dout!=0) err <= 1'b1;
                else err <= 1'b0;
            end
        end
    end

    crc #(
        .dat_width(16),
        .crc_width(6)
    )
    crc_inst(
        .din(din),
        .cin(cin),
        .dout(dout)
    );

endmodule 