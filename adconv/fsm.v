module fsm(clk, rst_n, data_in, clock, data_out, cs);

    input clk;
    input rst_n;
    input data_in;
    output reg clock;
    output reg [7:0] data_out;
    output reg cs;
    
    reg [7:0] data;
    reg [9:0] cnt;
    reg [3:0] state;
    
    always @(posedge clk or negedge rst_n)
    begin
        if(!rst_n)
            begin
                data_out <= 8'b0;
                data <= 8'b0;
                clock <= 0;
                cs <= 1;
                cnt <= 0;
                state <= 0;
            end
        else
            case(state)
                0: begin
                    cs <= 0;
                    if(cnt<70)
                        begin
                            cnt <=cnt + 1;
                            state <= 0;
                        end
                    else
                        begin
                            data[7] <= data_in;
                            clock <= 1;
                            cnt <= 0;
                            state <= 1;
                        end
                end
                1: begin
                    if(cnt<24)
                        begin
                            cnt <= cnt + 1;
                            state <= 1;
                        end
                    else if(cnt<49)
                        begin
                            clock <= 0;
                            cnt <= cnt + 1;
                            state <= 1;
                        end
                    else
                        begin
                            data[6] <= data_in;
                            clock <= 1;
                            cnt <= 0;
                            state <= 2;
                        end
                end
                2: begin
                    if(cnt<24)
                        begin
                            cnt <= cnt + 1;
                            state <= 2;
                        end
                    else if(cnt<49)
                        begin
                            clock <= 0;
                            cnt <= cnt + 1;
                            state <= 2;
                        end
                    else
                        begin
                            data[5] <= data_in;
                            clock <= 1;
                            cnt <= 0;
                            state <= 3;
                        end
                end
                3: begin
                    if(cnt<24)
                        begin
                            cnt <= cnt + 1;
                            state <= 3;
                        end
                    else if(cnt<49)
                        begin
                            clock <= 0;
                            cnt <= cnt + 1;
                            state <= 3;
                        end
                    else
                        begin
                            data[4] <= data_in;
                            clock <= 1;
                            cnt <= 0;
                            state <= 4;
                        end
                end
                4: begin
                    if(cnt<24)
                        begin
                            cnt <= cnt + 1;
                            state <= 4;
                        end
                    else if(cnt<49)
                        begin
                            clock <= 0;
                            cnt <= cnt + 1;
                            state <= 4;
                        end
                    else
                        begin
                            data[3] <= data_in;
                            clock <= 1;
                            cnt <= 0;
                            state <= 5;
                        end
                end
                5: begin
                    if(cnt<24)
                        begin
                            cnt <= cnt + 1;
                            state <= 5;
                        end
                    else if(cnt<49)
                        begin
                            clock <= 0;
                            cnt <= cnt + 1;
                            state <= 5;
                        end
                    else
                        begin
                            data[2] <= data_in;
                            clock <= 1;
                            cnt <= 0;
                            state <= 6;
                        end
                end
                6: begin
                    if(cnt<24)
                        begin
                            cnt <= cnt + 1;
                            state <= 6;
                        end
                    else if(cnt<49)
                        begin
                            clock <= 0;
                            cnt <= cnt + 1;
                            state <= 6;
                        end
                    else
                        begin
                            data[1] <= data_in;
                            clock <= 1;
                            cnt <= 0;
                            state <= 7;
                        end
                end
                7: begin
                    if(cnt<24)
                        begin
                            cnt <= cnt + 1;
                            state <= 7;
                        end
                    else if(cnt<49)
                        begin
                            clock <= 0;
                            cnt <= cnt + 1;
                            state <= 7;
                        end
                    else
                        begin
                            data[0] <= data_in;
                            clock <= 1;
                            cnt <= 0;
                            state <= 8;
                        end
                end
                8: begin
                    if(cnt<24)
                        begin
                            cnt <= cnt + 1;
                            state <= 8;
                        end
                    else if(cnt<873)
                        begin
                            cs <= 1;
                            clock <= 0;
                            data_out <= data;
                            cnt <= cnt + 1;
                            state <= 8;
                        end
                    else
                        begin
                            cnt <= 0;
                            state <= 0;
                        end
                end
                default: state <= 0;
            endcase
    end
    
endmodule 