`timescale 1ns/1ps

module ad_tb;

    reg clk;
    reg rst_n;
    reg data;
    wire clock;
    wire cs;
    wire [7:0] data_out;

    initial begin
        clk = 0;
        rst_n = 0;
        data = 0;
        #200.1 rst_n = 1;
        data = 1; #5000
        data = 0; #5000
        #20000 $stop;
    end
    
    always #10 clk = ~clk;
    
    lsm lsm(
        .clk(clk),
        .rst_n(rst_n),
        .data_in(data),
        .clock(clock),
        .data_out(data_out),
        .cs(cs)
    );
    
endmodule 