module lsm(clk, rst_n, data_in, clock, data_out, cs);

    input clk;
    input rst_n;
    input data_in;
    output reg clock;
    output reg [7:0] data_out;
    output reg cs;
    
    reg [7:0] data;
    reg [20:0] cnt;
    
    always @(posedge clk or negedge rst_n)
    begin
        if(!rst_n)
            cnt <= 0;
        else
            if(cnt<21'd1294)
                cnt <= cnt + 1'b1;
            else
                cnt <= 0;
    end
    
    always @(*)
    begin
        if(!rst_n)
            begin
                data_out = 8'b0;
                data = 8'b0;
                clock = 0;
                cs = 1;
            end
        else
            case(cnt)
                1  : begin cs = 0; clock = 0; end
                71 : begin clock = 1; data[7] = data_in; end
                96 : begin clock = 0; end
                121: begin clock = 1; data[6] = data_in; end
                146: begin clock = 0; end
                171: begin clock = 1; data[5] = data_in; end
                196: begin clock = 0; end
                221: begin clock = 1; data[4] = data_in; end
                246: begin clock = 0; end
                271: begin clock = 1; data[3] = data_in; end
                296: begin clock = 0; end
                321: begin clock = 1; data[2] = data_in; end
                346: begin clock = 0; end
                371: begin clock = 1; data[1] = data_in; end
                396: begin clock = 0; end
                421: begin clock = 1; data[0] = data_in; end
                446: begin clock = 0; cs = 1; data_out = data; end
                default: ;
            endcase
    end
    
endmodule 