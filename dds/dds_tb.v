`timescale 1ns/1ps

module dds_tb;
    
    reg clk;
    reg rst_n;
    wire [7:0] wout;
    
    initial begin
        clk = 0;
        rst_n = 0;
        #200.1 rst_n = 1;
    end 
    
    always #10 clk = ~clk;
    
    dds dut(
        .clk(clk),
        .rst_n(rst_n),
        .wout(wout)
    );
    
endmodule 