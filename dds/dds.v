module dds(
    input clk,
    input rst_n,
    output [7:0] wout
);
    
    reg  [7 :0] fword = 8'h01;
    reg  [7 :0] pword = 8'h00;
    reg  [31:0] addrs;
    wire [8 :0] addr;
    
    assign addr = addrs[31:23] + pword; //仿真时修改为addrs[15:7]
    
    always @(posedge clk, negedge rst_n) begin
        if (!rst_n) begin
            addrs <= 32'b0;
        end
        else begin
            addrs <= addrs + fword;
        end
    end
    
    wrom wrom(
        .clock(clk),
        .address(addr),
        .q(wout)
    );

endmodule 