`timescale 1ns/1ps

module page_tb;

    reg clk;
    reg rst_n;
    reg [15:0] addr;
    reg csen;
    reg wren;
    reg [7:0] din;
    wire [7:0] dout;
    wire scl;
    wire sda;
    
    reg flag;
    
    initial begin
        clk = 0;
        rst_n = 0;
        addr = 16'hAA55;
        csen = 1;
        wren = 0;
        flag = 0;
        din = 8'hA5;
        #200.1 rst_n = 1;
        
        #20   csen = 0; #20   csen = 1;
        #2410 flag = 1; #240  flag = 0;
        #1920 flag = 1; #240  flag = 0;
        #1920 flag = 1; #240  flag = 0;
        #2160 flag = 1; #67440 flag = 0;
        #600 wren = 1; csen = 0; #20 csen = 1;
        #2500 flag = 1; #240  flag = 0;
        #1920 flag = 1; #240  flag = 0;
        #1920 flag = 1; #240  flag = 0;
        #1920 flag = 1; #240  flag = 0;
        #1920 flag = 1; #240  flag = 0;
        #1920 flag = 1; #240  flag = 0;
        #1920 flag = 1; #240  flag = 0;
        #1920 flag = 1; #240  flag = 0;
        #1920 flag = 1; #240  flag = 0;
        #1920 flag = 1; #240  flag = 0;
        #1920 flag = 1; #240  flag = 0;
        #1920 flag = 1; #240  flag = 0;
        #1920 flag = 1; #240  flag = 0;
        #1920 flag = 1; #240  flag = 0;
        #1920 flag = 1; #240  flag = 0;
        #1920 flag = 1; #240  flag = 0;
        #1920 flag = 1; #240  flag = 0;
        #1920 flag = 1; #240  flag = 0;
        #1920 flag = 1; #240  flag = 0;
        #1920 flag = 1; #240  flag = 0;
        #1920 flag = 1; #240  flag = 0;
        #1920 flag = 1; #240  flag = 0;
        #1920 flag = 1; #240  flag = 0;
        #1920 flag = 1; #240  flag = 0;
        #1920 flag = 1; #240  flag = 0;
        #1920 flag = 1; #240  flag = 0;
        #1920 flag = 1; #240  flag = 0;
        #1920 flag = 1; #240  flag = 0;
        #1920 flag = 1; #240  flag = 0;
        #1920 flag = 1; #240  flag = 0;
        #1920 flag = 1; #240  flag = 0;
        #1920 flag = 1; #240  flag = 0;
        #1920 flag = 1; #240  flag = 0;
        #1920 flag = 1; #240  flag = 0;
        #1920 flag = 1; #240  flag = 0;
        #2000 $stop;
    end
    
    always #10 clk = ~clk;
    
    assign sda = flag ? 1'b1 : 1'bz;
    
    iicpage iicpage(
        .clk(clk), 
        .rst_n(rst_n), 
        .addr(addr), 
        .csen(csen), 
        .wren(wren), 
        .din(din), 
        .dout(dout), 
        .scl(scl), 
        .sda(sda)
    );
    
endmodule 