module char3S(clk, rst_n, a_flow, b_flow, out_flow);

    input clk;
    input rst_n;
    input [7:0] a_flow;
    input [7:0] b_flow;
    output reg [7:0] out_flow;

    reg [3:0] Cstate;
    reg [3:0] Nstate;
    
    always @(posedge clk or negedge rst_n)
    begin
        if(!rst_n)
            Cstate <= 0;
        else
            Cstate <= Nstate;
    end

    always @(*)
    begin
        case(Cstate)
            0: begin
                if(a_flow=="H")
                    Nstate <= 1;
                else
                    Nstate <= 0;
            end
            1: begin
                if(b_flow=="e")
                    Nstate <= 2;
                else
                    Nstate <= 1;
            end
            2: begin
                if(b_flow=="l")
                    Nstate <= 3;
                else
                    Nstate <= 2;
            end
            3: begin
                if(b_flow=="l")
                    Nstate <= 4;
                else
                    Nstate <= 3;
            end
            4: begin
                if(b_flow=="o")
                    Nstate <= 5;
                else
                    Nstate <= 4;
            end
            5: begin
                Nstate <= 6;
            end
            6: begin
                if(a_flow=="W")
                    Nstate <= 7;
                else
                    Nstate <= 6;
            end
            7: begin
                if(b_flow=="o")
                    Nstate <= 8;
                else
                    Nstate <= 7;
            end
            8: begin
                if(b_flow=="r")
                    Nstate <= 9;
                else
                    Nstate <= 8;
            end
            9: begin
                if(b_flow=="l")
                    Nstate <= 10;
                else
                    Nstate <= 9;
            end
            10: begin
                if(b_flow=="d")
                    Nstate <= 11;
                else
                    Nstate <= 10;
            end
            11: begin
                Nstate <= 0;
            end
            default: Nstate <= 0;
        endcase
    end
    
    always @(posedge clk or negedge rst_n)
    begin
        if(!rst_n)
            out_flow <= 0;
        else
            case(Cstate)
                0: begin
                    if(a_flow=="H")
                        out_flow <= a_flow;
                end
                1: begin
                    if(b_flow=="e")
                        out_flow <= b_flow;
                end
                2: begin
                    if(b_flow=="l")
                        out_flow <= b_flow;
                end
                3: begin
                    if(b_flow=="l")
                        out_flow <= b_flow;
                end
                4: begin
                    if(b_flow=="o")
                        out_flow <= b_flow;
                end
                5: begin
                    out_flow <= " ";
                end
                6: begin
                    if(a_flow=="W")
                        out_flow <= a_flow;
                end
                7: begin
                    if(b_flow=="o")
                        out_flow <= b_flow;
                end
                8: begin
                    if(b_flow=="r")
                        out_flow <= b_flow;
                end
                9: begin
                    if(b_flow=="l")
                        out_flow <= b_flow;
                end
                10: begin
                    if(b_flow=="d")
                        out_flow <= b_flow;
                end
                11: begin
                    out_flow <= "!";
                end
                default: ;
            endcase
        end
    
endmodule 