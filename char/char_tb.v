`timescale 1ns/1ps

module char_tb;

    reg clk;
    reg rst_n;
    reg [7:0] a_flow;
    reg [7:0] b_flow;
    wire [7:0] out_flow;

    reg [4:0] counter;

    initial begin
        clk = 0;
        rst_n = 0;
        #200.1 rst_n = 1;
        #20000 $stop;
    end

    always #10 clk = ~clk;

    always @(posedge clk or negedge rst_n)
    begin
        if(!rst_n)
            begin
                a_flow <= 0;
                b_flow <= 0;
                counter <= 0;
            end
        else
            if(counter<26)
                begin
                    a_flow <= counter + 65;
                    b_flow <= counter + 97;
                    counter <= counter + 1;
                end
            else
                counter <= 0;
    end
    
    char char(
        .clk(clk),
        .rst_n(rst_n),
        .a_flow(a_flow),
        .b_flow(b_flow),
        .out_flow(out_flow)
    );
    
endmodule 