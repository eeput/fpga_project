module char1S(clk, rst_n, a_flow, b_flow, out_flow);

    input clk;
    input rst_n;
    input [7:0] a_flow;
    input [7:0] b_flow;
    output reg [7:0] out_flow;

    reg [3:0] state;

    always @(posedge clk or negedge rst_n)
    begin
        if(!rst_n)
            begin
                out_flow <= 0;
                state <= 0;
            end
        else
            case(state)
                0: begin
                    if(a_flow=="H")
                        begin
                            out_flow <= a_flow;
                            state <= 1;
                        end
                    else
                        state <= 0;
                end
                1: begin
                    if(b_flow=="e")
                        begin
                            out_flow <= b_flow;
                            state <= 2;
                        end
                    else
                        state <= 1;
                end
                2: begin
                    if(b_flow=="l")
                        begin
                            out_flow <= b_flow;
                            state <= 3;
                        end
                    else
                        state <= 2;
                end
                3: begin
                    if(b_flow=="l")
                        begin
                            out_flow <= b_flow;
                            state <= 4;
                        end
                    else
                        state <= 3;
                end
                4: begin
                    if(b_flow=="o")
                        begin
                            out_flow <= b_flow;
                            state <= 5;
                        end
                    else
                        state <= 4;
                end
                5: begin
                    out_flow <= " ";
                    state <= 6;
                end
                6: begin
                    if(a_flow=="W")
                        begin
                            out_flow <= a_flow;
                            state <= 7;
                        end
                    else
                        state <= 6;
                end
                7: begin
                    if(b_flow=="o")
                        begin
                            out_flow <= b_flow;
                            state <= 8;
                        end
                    else
                        state <= 7;
                end
                8: begin
                    if(b_flow=="r")
                        begin
                            out_flow <= b_flow;
                            state <= 9;
                        end
                    else
                        state <= 8;
                end
                9: begin
                    if(b_flow=="l")
                        begin
                            out_flow <= b_flow;
                            state <= 10;
                        end
                    else
                        state <= 9;
                end
                10: begin
                    if(b_flow=="d")
                        begin
                            out_flow <= b_flow;
                            state <= 11;
                        end
                    else
                        state <= 10;
                end
                11: begin
                    out_flow <= "!";
                    state <= 0;
                end
                default: state <= 0;
            endcase
    end
    
endmodule 