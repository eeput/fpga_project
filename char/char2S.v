module char2S(clk, rst_n, a_flow, b_flow, out_flow);

    input clk;
    input rst_n;
    input [7:0] a_flow;
    input [7:0] b_flow;
    output reg [7:0] out_flow;

    reg [3:0] Cstate;
    reg [3:0] Nstate;
    
    always @(posedge clk or negedge rst_n)
    begin
        if(!rst_n)
            Cstate <= 0;
        else
            Cstate <= Nstate;
    end

    always @(*)
    begin
        case(Cstate)
            0: begin
                if(a_flow=="H")
                    begin
                        out_flow <= a_flow;
                        Nstate <= 1;
                    end
                else
                    Nstate <= 0;
            end
            1: begin
                if(b_flow=="e")
                    begin
                        out_flow <= b_flow;
                        Nstate <= 2;
                    end
                else
                    Nstate <= 1;
            end
            2: begin
                if(b_flow=="l")
                    begin
                        out_flow <= b_flow;
                        Nstate <= 3;
                    end
                else
                    Nstate <= 2;
            end
            3: begin
                if(b_flow=="l")
                    begin
                        out_flow <= b_flow;
                        Nstate <= 4;
                    end
                else
                    Nstate <= 3;
            end
            4: begin
                if(b_flow=="o")
                    begin
                        out_flow <= b_flow;
                        Nstate <= 5;
                    end
                else
                    Nstate <= 4;
            end
            5: begin
                out_flow <= " ";
                Nstate <= 6;
            end
            6: begin
                if(a_flow=="W")
                    begin
                        out_flow <= a_flow;
                        Nstate <= 7;
                    end
                else
                    Nstate <= 6;
            end
            7: begin
                if(b_flow=="o")
                    begin
                        out_flow <= b_flow;
                        Nstate <= 8;
                    end
                else
                    Nstate <= 7;
            end
            8: begin
                if(b_flow=="r")
                    begin
                        out_flow <= b_flow;
                        Nstate <= 9;
                    end
                else
                    Nstate <= 8;
            end
            9: begin
                if(b_flow=="l")
                    begin
                        out_flow <= b_flow;
                        Nstate <= 10;
                    end
                else
                    Nstate <= 9;
            end
            10: begin
                if(b_flow=="d")
                    begin
                        out_flow <= b_flow;
                        Nstate <= 11;
                    end
                else
                    Nstate <= 10;
            end
            11: begin
                out_flow <= "!";
                Nstate <= 0;
            end
            default: Nstate <= 0;
        endcase
    end
    
endmodule 