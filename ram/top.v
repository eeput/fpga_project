module top(clk, rst_n, q);

    input clk;
    input rst_n;
    output [7:0] q;
    wire wren;
    wire [7:0] data;
    wire [7:0] address;

    ram_control ram_control(
        .clk(clk), 
        .rst_n(rst_n), 
        .wren(wren), 
        .data(data), 
        .address(address)
    );

    ram ram_inst(
        .address(address),
        .clock(clk),
        .data(data),
        .wren(wren),
        .q(q)
    );
    
endmodule 