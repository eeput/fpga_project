module ram_control(clk, rst_n, wren, data, address);

    input clk;
    input rst_n;
    output reg wren;
    output reg [7:0] data;
    output reg [7:0] address;
    reg state;

    always @(posedge clk or negedge rst_n)
    begin
        if(!rst_n)
            begin
                wren <= 0;
                data <= 0;
                address <= 0;
                state <= 0;
            end
        else
            case(state)
                0: begin
                    if(address<255)
                        begin
                            wren <= 1;
                            address <= address + 1;
                            data <= data + 1;
                        end
                    else
                        begin
                            state <= 1;
                            address <= 0;
                            data <= 0;
                            wren <= 0;
                        end
                end
                1: begin
                    wren<=0;
                    if(address<255)
                        begin
                            address <= address + 1;
                        end
                    else
                        begin
                            state <= 0;
                            address <= 0;
                            wren <= 1;
                        end
                end
                default: state <= 0;
            endcase
    end
    
endmodule 