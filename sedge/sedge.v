module sedge(clk, rst_n, signal, pos_edge, neg_edge, pn_edge);
    
    input clk;
    input rst_n;
    input signal;
    output pos_edge;
    output neg_edge;
    output pn_edge;
    reg [1:0] sreg;
    
    always@(posedge clk or negedge rst_n)
    begin
        if(!rst_n)
            sreg <= 0;
        else
            begin
                sreg[1] <= signal;
                sreg[0] <= sreg[1];
            end
    end
    
    assign pos_edge = ~sreg[0] & sreg[1];
    assign neg_edge = ~sreg[1] & sreg[0];
    assign pn_edge  =  sreg[0] ^ sreg[1];
    
endmodule 