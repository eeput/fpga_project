`timescale 1ns/1ps

module sedge_tb;
    
    reg clk;
    reg rst_n;
    reg signal;
    wire pos_edge;
    wire neg_edge;
    wire pn_edge;
    
    initial begin
    clk = 1;
    rst_n = 0;
    signal = 0;
    #200.1 rst_n = 1;
    
    #200 signal = 1;
    #200 signal = 0;
    #2000 $stop;
    end
    
    always #10 clk = ~clk;
    
    sedge sedge(
        .clk(clk),
        .rst_n(rst_n),
        .signal(signal),
        .pos_edge(pos_edge),
        .neg_edge(neg_edge),
        .pn_edge(pn_edge)
    );
    
endmodule 