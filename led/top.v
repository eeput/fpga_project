module top(clk, rst_n, pio_led);

    input clk;
    input rst_n;
    output [3:0]pio_led;
    wire clk1m;
    wire clk_slow;
    
    led led(
        .clk(clk_slow), 
        .rst_n(rst_n), 
        .pio_led(pio_led)
    );
    
    freq freq(
        .clk(clk1m), 
        .rst_n(rst_n), 
        .clk_slow(clk_slow)
    );
    
    my_pll my_pll(
        .inclk0(clk),
        .c0(clk1m)
    );
    
endmodule 