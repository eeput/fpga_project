module freq(clk, rst_n, clk_slow);

    input clk;
    input rst_n;
    output reg clk_slow;

    reg [30:0] counter;

    always @(posedge clk or negedge rst_n)
    begin
        if(!rst_n)
            begin
                counter <= 0;
                clk_slow <= 0;
            end
        else
            if(counter<((1000000/2)-1))   //仿真时改为counter<2
                counter <= counter + 1;
            else
                begin
                    counter <= 0;
                    clk_slow <= ~clk_slow;
                end
    end
    
endmodule 