`timescale 1ns/1ps

module led_tb;

    reg clk;
    reg rst_n;
    wire [3:0] pio_led;
    
    initial begin
        clk = 0;
        rst_n = 0;
        #200.1 rst_n = 1;
        #2000 $stop;
    end
    
    always #10 clk = ~clk;
    
    top top(
        .clk(clk), 
        .rst_n(rst_n), 
        .pio_led(pio_led)
    );
    
endmodule 