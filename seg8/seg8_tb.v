`timescale 1ns/1ps

module seg8_tb;
    
    reg clk;
    reg rst_n;
    wire [2:0] sel;
    wire [7:0] seg;
    
    initial begin
        clk = 0;
        rst_n = 0;
        #200.1 rst_n = 1;
        #20000 $stop;
    end
    
    always #10 clk = ~clk;
    
    seg8 dut(
        .clk(clk), 
        .rst_n(rst_n), 
        .sel(sel), 
        .seg(seg)
    );
    
endmodule 