module seg8(clk, rst_n, sel, seg);

    input clk;
    input rst_n;
    output reg [2:0] sel;
    output reg [7:0] seg;
    
    reg clk1K;
    reg [3:0] seg_buff;
    reg [20:0] counter;
    reg [23:0] data;

    always @(posedge clk or negedge rst_n)
    begin
        if(!rst_n)
            begin
                counter <= 0;
                clk1K <= 0;
            end
        else
            if(counter<(50000000/1000/2-1)) //仿真时改为counter<2
                counter <= counter + 1;
            else
                begin
                    counter <= 0;
                    clk1K <= ~clk1K;
                end
    end
    
    always @(posedge clk or negedge rst_n)
    begin
        if(!rst_n)
            data <= 0;
        else
            data <= data + 1;
    end
    
    always @(posedge clk1K or negedge rst_n)
    begin
        if(!rst_n)
            sel <= 0;
        else
            if(sel<5)
                sel <= sel + 1;
            else
                sel <= 0;
    end
    
    always @(*)
    begin
        if(!rst_n)
            seg_buff = 0;
        else
            case(sel)
                0: seg_buff  =  data[23:20];
                1: seg_buff  =  data[19:16];
                2: seg_buff  =  data[15:12];
                3: seg_buff  =  data[11:8];
                4: seg_buff  =  data[7:4];
                5: seg_buff  =  data[3:0];
                default: ;
            endcase
    end
    
    always @(*) //共阳极译码
    begin
        if(!rst_n)
            seg = 0;
        else
            case(seg_buff)
                4'd0 : seg = 8'hC0;
                4'd1 : seg = 8'hF9;
                4'd2 : seg = 8'hA4;
                4'd3 : seg = 8'hB0;
                4'd4 : seg = 8'h99;
                4'd5 : seg = 8'h92;
                4'd6 : seg = 8'h82;
                4'd7 : seg = 8'hF8;
                4'd8 : seg = 8'h80;
                4'd9 : seg = 8'h90;
                4'd10: seg = 8'h88;
                4'd11: seg = 8'h83;
                4'd12: seg = 8'hC6;
                4'd13: seg = 8'hA1;
                4'd14: seg = 8'h86;
                4'd15: seg = 8'h8E;
                default: ;
            endcase
    end
    
//    always @(*) //共阴极译码
//    begin
//        if(!rst_n)
//            seg = 0;
//        else
//            case(seg_buff)
//                4'd0 : seg = 8'h3F;
//                4'd1 : seg = 8'h06;
//                4'd2 : seg = 8'h5B;
//                4'd3 : seg = 8'h4F;
//                4'd4 : seg = 8'h66;
//                4'd5 : seg = 8'h6D;
//                4'd6 : seg = 8'h7D;
//                4'd7 : seg = 8'h07;
//                4'd8 : seg = 8'h7F;
//                4'd9 : seg = 8'h6F;
//                4'd10: seg = 8'h77;
//                4'd11: seg = 8'h7C;
//                4'd12: seg = 8'h39;
//                4'd13: seg = 8'h5E;
//                4'd14: seg = 8'h79;
//                4'd15: seg = 8'h71;
//                default: ;
//            endcase
//    end
    
endmodule 