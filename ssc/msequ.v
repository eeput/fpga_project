module msequ(
    input        clk,
    input        rst_n,
    input        sin,
    output [1:0] sout
);

    reg       dreg;
    reg [5:0] mreg;

    always @(posedge clk, negedge rst_n) begin
        if (!rst_n) begin
            mreg <= 6'b1;
            dreg <= 1'b0;
        end
        else begin
            mreg <= {(mreg[5] ^ mreg[0]), mreg[5:1]};
            dreg <= sin ^ mreg[0];
        end
    end

    assign sout = dreg ? 2'b01 : 2'b11;

endmodule 