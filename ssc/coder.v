module coder(
    input       clk,
    input       rst_n,
    input [7:0] sin,
    output      sout
);

    reg  [11:0] dreg;
    reg  [3 :0] cnt;
    wire [3 :0] creg;

    always @(posedge clk, negedge rst_n) begin
        if (!rst_n) begin
            dreg <= 12'hFFE;
            cnt <= 4'b0;
        end
        else if (cnt<11) begin
            dreg <= {dreg[10:0], 1'b0};
            cnt <= cnt + 1'b1;
        end
        else begin
            dreg <= {creg[3], creg[2], sin[7], creg[1], sin[6], sin[5], sin[4], creg[0], sin[3], sin[2], sin[1], sin[0]};
            cnt <= 4'b0;
        end
    end

    assign sout = dreg[11];

    assign creg[3] = sin[7] ^ sin[6] ^ sin[4] ^ sin[3] ^ sin[1];
    assign creg[2] = sin[7] ^ sin[5] ^ sin[4] ^ sin[2] ^ sin[1];
    assign creg[1] = sin[6] ^ sin[5] ^ sin[4] ^ sin[0];
    assign creg[0] = sin[3] ^ sin[2] ^ sin[1] ^ sin[0];

endmodule 