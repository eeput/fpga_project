module ssc(
    input        clk,
    input        clks,
    input        send_en,
    input        rec_en,
    input  [7:0] sin,
    input  [2:0] cin,
    output [1:0] cout,
    output [7:0] sout
);

    wire cdat;
    wire sdat;
    wire valid;

    coder coder(
        .clk(clks),
        .rst_n(send_en),
        .sin(sin),
        .sout(cdat)
    );

    msequ msequ(
        .clk(clk),
        .rst_n(send_en),
        .sin(cdat),
        .sout(cout)
    );

    msync msync(
        .clk(clk),
        .rst_n(rec_en),
        .sin(cin),
        .sout(sdat),
        .valid(valid)
    );

    decoder decoder(
        .clk(clks),
        .rst_n(rec_en),
        .sin(sdat),
        .valid(valid),
        .sout(sout)
    );

endmodule 