`timescale 1ns/1ps

module ssc_tb;

    reg        clk;
    reg        rst_n;
    reg  [7:0] sin;
    wire [2:0] cin;
    wire [1:0] cout;
    wire [7:0] sout;

    reg        clks;
    reg        send_en;
    reg        rec_en;
    reg [1 :0] noise;
    reg [31:0] cnt0;
    reg [3 :0] cnt1;

    initial begin
        clk = 1;
        rst_n = 0;
        send_en = 0;
        rec_en = 0;
        #200.1 rst_n = 1;
        #200 send_en = 1;
        #800 rec_en = 1;
    end

    always #10 clk = ~clk;

    always @(posedge clk, negedge rst_n) begin
        if (!rst_n) begin
            clks <= 1'b0;
            cnt0 <= 32'h8FFFFFFF;
        end
        else begin
            clks <= cnt0[31];
            cnt0 <= cnt0 + 32'h04104104;
        end
    end

    always @(posedge clks, negedge rst_n) begin
        if (!rst_n) begin
            sin <= 8'b0;
            cnt1 <= 4'b0;
        end
        else if (cnt1<11) begin
            cnt1 <= cnt1 + 1'b1;
        end
        else begin
            sin <= sin + 1'b1;
            cnt1 <= 4'b0;
        end
    end

    always @(posedge clk or negedge rst_n) begin
        if (!rst_n) noise <= 2'b0;
        else noise <= $random%3;
    end

    assign cin = {cout[1], cout} + noise;

    ssc dut(
        .clk(clk),
        .clks(clks),
        .send_en(send_en),
        .rec_en(rec_en),
        .sin(sin),
        .cin(cin),
        .cout(cout),
        .sout(sout)
    );

endmodule 