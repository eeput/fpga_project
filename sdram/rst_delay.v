module rst_delay(
    input       clk,
    input       global_rst,
    input       locked,
    output reg  soft_rst
);

    reg r0, r1;

    always @(posedge clk, posedge global_rst)
    begin
        if (global_rst) begin
            soft_rst <= 1'b0;
            r0 <= 1'b0;
            r1 <= 1'b0;
        end
        else begin
            r0 <= locked;
            r1 <= r0;
            soft_rst <= r1;
        end
    end

endmodule 