module sdr_ref_time(
    input       clk,
    input       soft_rst,
    input       ref_time_en,
    output reg  time_flag
);

    reg [9 : 0] count;

    always @(posedge clk)
    begin
        if (!soft_rst) begin
            time_flag <= 1'b0;
            count <= 10'b0;
        end
        else begin
            if (ref_time_en)
                count <= count + 1'b1;
            else
                count <= 10'b0;
            if (count == 750)
                time_flag <= 1'b1;
            else
                time_flag <= 1'b0;
        end
    end

endmodule 