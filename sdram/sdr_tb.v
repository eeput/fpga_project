`timescale 1ns/1ps

module sdr_tb;

    reg                 ref_clk, global_rst;
    reg     [24 : 0]    local_addr;
    reg     [31 : 0]    local_wdata;
    reg                 local_rdreq, local_wrreq;
    wire                sdr_cs_n, sdr_ras_n, sdr_cas_n, sdr_we_n;
    wire                sdr_cke, sdr_clk;
    wire    [12 : 0]    sdr_a;
    wire    [1  : 0]    sdr_ba;
    wire    [15 : 0]    sdr_dq;
    wire    [1  : 0]    sdr_dqm;
    wire    [31 : 0]    local_rdata;
    wire                local_ready;

    initial begin
        ref_clk = 1'b0;
        global_rst = 1'b1;
        local_addr = 25'b0;
        local_wdata = 32'h00f0_000f;
        local_rdreq = 1'b0;
        local_wrreq = 1'b0;
        #500.1 global_rst = 1'b0;

        #101000 local_wrreq = 1'b1;
        #10 local_wrreq = 1'b0;

        #100 local_addr = 25'd5;
             local_wdata = 32'hf000_0f00;
             local_wrreq = 1'b1;
        #10 local_wrreq = 1'b0;

        #200 local_addr = 25'd0;
             local_rdreq = 1'b1;
        #10 local_rdreq = 1'b0;

        #200 local_addr = 25'd5;
             local_rdreq = 1'b1;
        #10 local_rdreq = 1'b0;

        #20000 $stop;
    end

    always #10 ref_clk = ~ ref_clk;

    sdr_control control_inst(
        .ref_clk(ref_clk),
        .global_rst(global_rst),
        .sdr_cs_n(sdr_cs_n),
        .sdr_ras_n(sdr_ras_n),
        .sdr_cas_n(sdr_cas_n),
        .sdr_we_n(sdr_we_n),
        .sdr_a(sdr_a),
        .sdr_ba(sdr_ba),
        .sdr_clk(sdr_clk),
        .sdr_cke(sdr_cke),
        .local_addr(local_addr),
        .local_wdata(local_wdata),
        .local_rdata(local_rdata),
        .local_rdreq(local_rdreq),
        .local_wrreq(local_wrreq),
        .local_ready(local_ready),
        .sdr_dqm(sdr_dqm),
        .sdr_dq(sdr_dq)
    );

    sdr_device device_inst(
        .Dq(sdr_dq),
        .Addr(sdr_a),
        .Ba(sdr_ba),
        .Clk(sdr_clk),
        .Cke(sdr_cke),
        .Cs_n(sdr_cs_n),
        .Ras_n(sdr_ras_n),
        .Cas_n(sdr_cas_n),
        .We_n(sdr_we_n),
        .Dqm(sdr_dqm)
    );

endmodule 