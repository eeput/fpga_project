`define     CODE        13'b0_0000_0010_0001
`define     INH         4'b1000     // INH宏定义
`define     NOP         4'b0111     // NOP宏定义
`define     PRE         4'b0010     // PRE宏定义
`define     TRP         4'b0010     // TRP宏定义
`define     REF         4'b0001     // REF宏定义
`define     TRFC        4'b0111     // TRFC宏定义
`define     LMR         4'b0000     // LMR宏定义
`define     TWR         4'b0001     // TER宏定义
`define     TMRD        4'b0010     // TMRD宏定义
`define     TRCD        4'b0010     // TRCD宏定义
`define     T100us      15'd9999    // T100us宏定义
`define     ACT         4'b0011     // Active command
`define     RD          4'b0101     // Read command
`define     WR          4'b0100     // Write commmand
`define     BT          4'b0110     // Burst Terminate command
`define     SL          4'b0010     // Burst Terminate command
`define     CL          4'b0010     // Burst Terminate command
`define     INIT_MUX    2'b00
`define     REF_MUX     2'b01
`define     RD_MUX      2'b10
`define     WR_MUX      2'b11