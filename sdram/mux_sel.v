`include "sdr_head.v"

module mux_sel(
    input       [1  : 0]    mux_sel,
    input       [19 : 0]    init_bus,
    input       [19 : 0]    ref_bus,
    input       [19 : 0]    rd_bus,
    input       [19 : 0]    wr_bus,
    output reg  [19 : 0]    mux_bus
);

    always @(*)
    begin
        case (mux_sel)
            `INIT_MUX   : mux_bus = init_bus;
            `REF_MUX    : mux_bus = ref_bus;
            `RD_MUX     : mux_bus = rd_bus;
            `WR_MUX     : mux_bus = wr_bus;
            default     : mux_bus = init_bus;
        endcase
    end

endmodule 