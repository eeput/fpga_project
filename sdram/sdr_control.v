module sdr_control(
    input               ref_clk,
    input               global_rst,
    input   [24 : 0]    local_addr,
    input   [31 : 0]    local_wdata,
    input               local_rdreq,
    input               local_wrreq,
    output              local_ready,
    output  [31 : 0]    local_rdata,
    output              sdr_cs_n,
    output              sdr_ras_n,
    output              sdr_cas_n,
    output              sdr_we_n,
    output              sdr_clk,
    output              sdr_cke,
    output  [12 : 0]    sdr_a,
    output  [1  : 0]    sdr_dqm,
    output  [1  : 0]    sdr_ba,
    inout   [15 : 0]    sdr_dq
);

    wire                clk, locked;
    wire    [1  : 0]    mux_sel;
    wire    [24 : 0]    int_addr;
    wire    [31 : 0]    wdata, rdata;
    wire    [19 : 0]    init_bus, ref_bus, mux_bus, rd_bus, wr_bus;
    wire                init_done, init_en;
    wire                read_done, write_done;
    wire                read_en, write_en;
    wire                ref_done, ref_en;
    wire                ref_time_en;
    wire                time_flag;
    wire                cap_clk;

    assign {sdr_cke, sdr_cs_n, sdr_ras_n, sdr_cas_n, sdr_we_n, sdr_a, sdr_ba} = mux_bus;
    assign sdr_dqm = 2'b0;

    sdr_pll pll_inst(
        .areset(global_rst),
        .inclk0(ref_clk),
        .c0(clk),
        .c1(sdr_clk),
        .c2(cap_clk),
        .locked(locked)
    );

    rst_delay rst_inst(
        .clk(clk),
        .global_rst(global_rst),
        .locked(locked),
        .soft_rst(soft_rst)
    );

    sdr_init init_inst(
        .clk(clk),
        .soft_rst(soft_rst),
        .init_done(init_done),
         .init_bus(init_bus)
    );

    sdr_ref ref_inst(
        .clk(clk),
        .ref_en(ref_en),
        .ref_done(ref_done),
        .ref_bus(ref_bus)
    );

    mux_sel mux_inst(
        .mux_sel(mux_sel),
        .init_bus(init_bus),
        .ref_bus(ref_bus),
        .rd_bus(rd_bus),
        .wr_bus(wr_bus),
        .mux_bus(mux_bus)
    );

    sdr_fsm fsm_inst(
        .clk(clk),
        .soft_rst(soft_rst),
        .local_rdreq(local_rdreq),
        .local_wrreq(local_wrreq),
        .init_done(init_done),
        .ref_done(ref_done),
        .read_done(read_done),
        .write_done(write_done),
        .time_flag(time_flag),
        .local_wdata(local_wdata),
        .local_rdata(local_rdata),
        .local_ready(local_ready),
        .local_addr(local_addr),
        .mux_sel(mux_sel),
        .ref_en(ref_en),
        .init_en(init_en),
        .read_en(read_en),
        .write_en(write_en),
        .ref_time_en(ref_time_en),
        .wdata(wdata),
        .rdata(rdata),
        .int_addr(int_addr)
    );

    sdr_write write_inst(
        .clk(clk),
        .write_en(write_en),
        .write_done(write_done),
        .int_addr(int_addr),
        .wdata(wdata),
        .wr_bus(wr_bus),
        .sdr_dq(sdr_dq)
    );

    sdr_read read_inst(
        .clk(clk),
        .cap_clk(cap_clk),
        .read_en(read_en),
        .read_done(read_done),
        .int_addr(int_addr),
        .rdata(rdata),
        .rd_bus(rd_bus),
        .sdr_dq(sdr_dq)
    );

    sdr_ref_time time_inst(
        .clk(clk),
        .soft_rst(soft_rst),
        .ref_time_en(ref_time_en),
        .time_flag(time_flag)
    );

endmodule 